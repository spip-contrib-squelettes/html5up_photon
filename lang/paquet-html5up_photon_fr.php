<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// H
	'html5up_photon_description' => 'Intégration à SPIP du modèle Photon (HTML5UP)',
	'html5up_photon_nom' => 'Html5up Photon',
	'html5up_photon_slogan' => 'Squelette responsive «Photon» de HTML5UP',
);
