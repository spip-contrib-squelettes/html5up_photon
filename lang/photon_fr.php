<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	// A
	'aide' => 'Aide : <a href="">lire la documentation sur contrib.spip.net</a>',
	'article_one_label' => 'Sélectionner le premier article de la page principale',
	'article_one_explication' => 'Numéro d’article choisi',
	'article_two_label' => 'Sélectionner le deuxième article de la page principale',
	'article_two_explication' => 'Numéro d’article choisi',
	
	// C
	'choix_favicon' => 'Choix des Favicon de la section "Two"',
	
	//D
	'Decouvrir' => 'Découvrez',
	
	//F
	'favicon_1' => 'Première favicon',
	'favicon_2' => 'Deuxième favicon',
	'favicon_3' => 'Troisième favicon',
	'favicon_4' => 'Quatrième favicon',
	'favicon_5' => 'Cinquième favicon',
	'favicon_6' => 'Sixième favicon',
	
	
	// H
	'html5up_photon_titre' => 'Html5up Photon',
	'lire_la_suite' => 'Lire la suite',
	
	//M
	'main_image_label' => 'Image de backround page principale',
	'main_image_explication' => "Numéro du document image utilisé en entête. Taille minimale : 2000px de large",
	
	// T
	'titre_page_configurer_html5up_photon' => 'Configurer le squelette Photon',
);
