<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_titre_parametrages' => 'Paramétrages',
	'contact' => 'Restez en contact',
	'contact_tel' => 'Téléphone',
	'contact_adresse' => 'Adresse',
	'contact_complement' => 'Complément',

	
	// D
	"design" => 'Design',
	

		
	// R
	'rechercher' => 'Chercher',

	// S
	'suivre' => 'Suivre',
	

);
