<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Html5up Photon
 *
 * @plugin     Html5up Photon
 * @copyright  2017
 * @author     Jack31
 * @licence    GNU/GPL
 * @package    SPIP\Html5up_photon\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Html5up Photon.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function html5up_photon_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	$maj['create'] = [['html5up_photon_creer_formulaire']];


	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Installer le formulaire formidable livré avec le plugin.
 * @return void
**/
function html5up_photon_creer_formulaire() {
	include_spip('inc/formidable');
	include_spip('inc/config');
	include_spip('inc/sql');
	include_spip('formidable_fonctions');
	$importer = charger_fonction('importer', "echanger/formulaire/yaml", true);
	$id_formulaire = $importer(_DIR_PLUGIN_HTML5UP_PHOTON.'formidable/formulaire-contact.yaml');

	$traitements = sql_getfetsel('traitements', 'spip_formulaires', "id_formulaire=$id_formulaire");
	$traitements = formidable_deserialize($traitements);
	$traitements['email']['destinataires_plus'] = lire_config('email_webmaster');
	$traitements = formidable_serialize($traitements);
	sql_updateq('spip_formulaires', ['traitements' => $traitements], "id_formulaire=$id_formulaire");

}
/**
 * Fonction de désinstallation du plugin Html5up Photon .
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function html5up_photon_vider_tables($nom_meta_base_version) {
	effacer_meta($nom_meta_base_version);
	effacer_meta('html5up');
}
