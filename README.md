# Squelette HTML5UP PHOTON

Le but est de faire un squelette simple un One Page de présentation mais avec quelques articles pour afficher l'activité d'une petite structure (un petit club d'échecs au départ)


## Squelettes fournis
En plus des éléments qui apparaissent sur le "One-page d'accueil :
- article.html à la racine  (html5up_photon ne propose de page générique)
- plan.html
Les pages non utilisées (auteurs.html, mots.html, rubrique.html) sont surchargées pour ne pas être appelées


## Formulaire de contact
Suite à cette discussion https://git.spip.net/spip-contrib-extensions/contact/issues/2 on utilise Formidable comme formulaire de contact.

Un formulaire de contact basique est proposé dans un dossier formidable, à l'installation :
- le formulaire est uploadé automatiquement
- l'adresse du webmaster (auteur 1) vient en destinataire des mails

Par ailleurs le formulaire de contact est dans la section "four" et non dans le footer

## Paramétrages

Le formulaire de configuration permet de:
- Choisir l'image de background dans la médiathèque
- Choisir dans les articles publiés ceux qui seront dans la section One et dans la section Two (obligatoire pour que la mise en page se mette en place correctement)
- Choisir deux à six favicon à choisir dans la page de démo
- il est possible de paramétrer des champs de contact tel, adresse et d'autres infos
- le formulaire de contact est chargé en auto à l'install du squelette


## Image de background
Apparemment l'image du thème original a déjà un effet de blur, non prévu dans le thème. Afin de rendre le texte lisible sous l'image j'ai utilisé la première solution trouvée ici : https://css-tricks.com/design-considerations-text-images/
Mais on peut sans doute faire mieux

## Réseaux sociaux
Les favicon des réseaux sociaux paramétrés apparaissent dans le footer

## Pages uniques

Il est possible de créer une page "Mentions légales" en créant une page unique et en la nommant "mentions"
